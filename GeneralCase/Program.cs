﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCase
{
    class Program
    {
        static void Main(string[] args)
        {
            //BucketCreationPhase();
            //RunSimulationPhase();
            //EstimateEncryptedTimes();

            EstimateBetaValue();
        }

        private static void EstimateBetaValue()
        {
            Benchmark.EstimateBeta eb = new Benchmark.EstimateBeta();
            eb.run();
            eb.print();
        }

        private static void RunSimulationPhase()
        {
            Benchmark.Simulation.SetupConnection();

            Benchmark.Simulation sm = null;

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_10PERCENT", "CUSTOMER_NS_90PERCENT", "10");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_20PERCENT", "CUSTOMER_NS_80PERCENT", "20");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_30PERCENT", "CUSTOMER_NS_70PERCENT", "30");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_40PERCENT", "CUSTOMER_NS_60PERCENT", "40");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_50PERCENT", "CUSTOMER_NS_50PERCENT", "50");


            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_60PERCENT", "CUSTOMER_NS_40PERCENT", "60");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_70PERCENT", "CUSTOMER_NS_30PERCENT", "70");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_80PERCENT", "CUSTOMER_NS_20PERCENT", "80");

            //sm = new Benchmark.Simulation();
            //sm.perform("CUSTOMER_S_90PERCENT", "CUSTOMER_NS_10PERCENT", "90");




        }

        private static void BucketCreationPhase()
        {
            BucketCreation.CreateBuckets.SetupConnection();

            BucketCreation.CreateBuckets cb = null;

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_10PERCENT", "CUSTOMER_NS_90PERCENT", "10");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_20PERCENT", "CUSTOMER_NS_80PERCENT", "20");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_30PERCENT", "CUSTOMER_NS_70PERCENT", "30");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_40PERCENT", "CUSTOMER_NS_60PERCENT", "40");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_50PERCENT", "CUSTOMER_NS_50PERCENT", "50");


            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_60PERCENT", "CUSTOMER_NS_40PERCENT", "60");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_70PERCENT", "CUSTOMER_NS_30PERCENT", "70");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_80PERCENT", "CUSTOMER_NS_20PERCENT", "80");

            //cb = new BucketCreation.CreateBuckets();
            //cb.StartCreatingBuckets("CUSTOMER_S_90PERCENT", "CUSTOMER_NS_10PERCENT", "90");



        }

        private static void EstimateEncryptedTimes()
        {
            Benchmark.EstimateDecryptionTime.SetupConnection();
            Benchmark.EstimateDecryptionTime.SetupConnectionNoEncryption();

            Benchmark.EstimateDecryptionTime ed = new Benchmark.EstimateDecryptionTime();
            ed.perform();

        }
    }
}
