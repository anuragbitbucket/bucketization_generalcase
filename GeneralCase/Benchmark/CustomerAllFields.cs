﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCase.Benchmark
{
    class CustomerAllFields
    {
        public decimal CustKey { get; set; }
        public string CustName { get; set; }
        public string Address { get; set; }
        public decimal NationKey { get; set; }
        public string Phone { get; set; }
        public decimal AccBalance { get; set; }
        public string MarketSgmt { get; set; }
        public string Comment { get; set; }

    }
}
