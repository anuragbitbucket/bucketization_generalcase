﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCase.Benchmark
{
    class EstimateDecryptionTime
    {

        private static int NUMBER_OF_SIMULATION = 20;
        private int NUMBER_OF_COLS = 150000;
        private long[] NoDecryptionCustKey = new long[NUMBER_OF_SIMULATION];
        private long[] WithDecryptionCustKey = new long[NUMBER_OF_SIMULATION];
        private decimal[] Keys = new decimal[NUMBER_OF_SIMULATION];
        private int currentSimulation = -1;


        public void perform()
        {
            Random rnd = new Random();
            for (int i=0; i< NUMBER_OF_SIMULATION; i++)
            {
                this.currentSimulation = i;
                decimal CustKey = rnd.Next(1, 100000 + 1);
                Keys[i] = CustKey;
                CleanBuffers();
                GetRIDFromFullSensitiveTableNoDecryption(CustKey);

            }
            for (int i = 0; i < NUMBER_OF_SIMULATION; i++)
            {
                this.currentSimulation = i;
                CleanBuffers();
                GetRIDFromFullSensitiveTableWithDecrytion(Keys[i]);

            }
            using (System.IO.StreamWriter file =
                 new System.IO.StreamWriter("DecryptionTime.txt"))
            {
                file.WriteLine(WithDecryptionCustKey.Average() + ":" + NoDecryptionCustKey.Average());
            }
        }

        private List<Byte[]> GetRIDFromFullSensitiveTableNoDecryption(decimal CustKeyToFind)
        {

            List<Byte[]> RIDList = new List<Byte[]>();

            List<decimal> tempKeys = new List<decimal>(NUMBER_OF_SIMULATION * 2);
            List<Byte[]> tempRids = new List<byte[]>(NUMBER_OF_SIMULATION*2);
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] , %%physloc%% AS [RID] FROM [GENERALCASE].[CUSTOMER_TEMP] ",
                new SqlConnection(connectionStringNoEncryption));
            using (sqlCmd.Connection = new SqlConnection(connectionStringNoEncryption))
            {

                sqlCmd.CommandTimeout = 600;
                Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            tempKeys.Add(reader.GetDecimal(0));
                            //tempRids.Add((byte[])reader[0]);
                            tempRids.Add((byte[])reader[1]);

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw1.Stop();
                NoDecryptionCustKey[this.currentSimulation] = sw1.ElapsedMilliseconds;

                Stopwatch sw2 = System.Diagnostics.Stopwatch.StartNew();
                for (int i = 0; i < tempKeys.Count; i++)
                {
                    if (tempKeys[i] == CustKeyToFind)
                    {
                        RIDList.Add(tempRids[i]);
                    }
                }
                sw2.Stop();


            }

            return RIDList;


        }

        private List<Byte[]> GetRIDFromFullSensitiveTableWithDecrytion(decimal CustKeyToFind)
        {

            List<Byte[]> RIDList = new List<Byte[]>();

            List<decimal> tempKeys = new List<decimal>(NUMBER_OF_SIMULATION * 2);
            List<Byte[]> tempRids = new List<byte[]>(NUMBER_OF_SIMULATION * 2);
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] , %%physloc%% AS [RID] FROM [GENERALCASE].[FULLTABLE_ENCRYPTED] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                sqlCmd.CommandTimeout = 600;
                Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            tempKeys.Add(reader.GetDecimal(0));
                            tempRids.Add((byte[])reader[1]);

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw1.Stop();
                WithDecryptionCustKey[this.currentSimulation] = sw1.ElapsedMilliseconds;

                Stopwatch sw2 = System.Diagnostics.Stopwatch.StartNew();
                for(int i=0; i<tempKeys.Count; i++)
                {
                    if(tempKeys[i] == CustKeyToFind)
                    {
                        RIDList.Add(tempRids[i]);
                    }
                }
                sw2.Stop();
                

            }

            return RIDList;


        }

        static string connectionString = @"GeneralCase.Properties.Settings.SQLServerConn";
        static string connectionStringNoEncryption = @"GeneralCase.Properties.Settings.SQLServerConn";
        public static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }


        public static void SetupConnectionNoEncryption()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionStringNoEncryption = sConnection;


            Console.WriteLine(connectionStringNoEncryption);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionStringNoEncryption);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            //connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionStringNoEncryption = connStringBuilder.ConnectionString;
        }

        private void CleanBuffers()
        {
            string FlushStatement = "DBCC DROPCLEANBUFFERS; ";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.Connection.Open();

                    cmd.CommandText = FlushStatement;

                    cmd.ExecuteNonQuery();

                    //Console.WriteLine("Buffer Cleaned");
                }
            }
        }
    }
}
