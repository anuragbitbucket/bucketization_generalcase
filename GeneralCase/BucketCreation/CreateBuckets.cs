﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UnequalPartition.Utility;

namespace GeneralCase.BucketCreation
{
    public class CreateBuckets
    {
        
        //private int SSize = -1;//250
        //private int NSSize = -1;//380
        private int X;
        private int Y;
        private bool NSIsLarger = true;

        private string SensitiveTableName = "";
        private string NonSenstitveTableName = "";
        private string SensitivityPercent = "";




        private Random random = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private decimal NextFakeKey = -1;
        private Dictionary<decimal, int> SKeyBucketDict = new Dictionary<decimal, int>();
        private List<List<decimal>> SKeyBucketsList = new List<List<decimal>>();
        private List<int> SizeOfSensBuckets_Keys = new List<int>();
        private Dictionary<decimal, int> NSKeyBucketDict = new Dictionary<decimal, int>();
        private List<List<decimal>> NSKeyBucketsList = new List<List<decimal>>();
        private List<int> SizeOfNonSensBuckets_Values = new List<int>();


        public void StartCreatingBuckets(
            string sensitiveTableName,
            string nonSensitiveTableName,
            string sensitivityPercent
            )
        {
            this.SensitiveTableName = sensitiveTableName;
            this.NonSenstitveTableName = nonSensitiveTableName;
            this.SensitivityPercent = sensitivityPercent;


            using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(@"Factors.txt", true))
            {
                

                long nsValCount = GetDistinctCustCount(this.NonSenstitveTableName);
                long sValCount = GetDistinctCustCount(this.SensitiveTableName);
                if(nsValCount > sValCount)
                {
                    this.NSIsLarger = true;
                }
                else
                {
                    this.NSIsLarger = false;
                }
                

                if(this.NSIsLarger == true)//ns is larger case
                {
                    Factors factors = Utils.SmallestDiffFactors(nsValCount);
                    {
                        double cost1 = sValCount / (double)factors.x + factors.x;
                        int x = (int)Math.Sqrt(nsValCount);
                        //double cost2 = sValCount / (double)x + x;
                        double cost3 = sValCount / (double)(x + 1) + (x + 1);
                        file.WriteLine(DateTime.Now.ToString() + "\n");
                        file.WriteLine("Sensitive Value Count:" + sValCount + "\t" + "NonSensitive Value Count:" + nsValCount + "\n");
                        file.WriteLine(cost1 + " : " + cost3);

                        if (//cost1 < cost2 && 
                            cost1 < cost3)
                        {
                            this.Y = (int)factors.y;
                            this.X = (int)factors.x;
                        }
                        //else if (cost2 < cost3 && cost2 < cost1)
                        //{
                        //    this.Y = x;
                        //    this.X = x;
                        //}
                        else
                        {
                            this.Y = x + 1;
                            this.X = x + 1;
                        }

                        file.WriteLine("X=" + this.X + " : " + "Y=" + this.Y);
                        file.WriteLine("\n\n");

                    }
                }
                else
                {
                    // in this scenario y is the enforcer
                    //it forces that max y values can be in each bucket for S
                    //and that a minimum of y buckets are created in NS side
                    Factors factors = Utils.SmallestDiffFactors(sValCount);
                    {
                        double cost1 =factors.y + (double)nsValCount/(double)factors.y;
                        int y = (int)Math.Sqrt(sValCount);
                        double cost2 = y + (double)nsValCount / (double)y;
                        //double cost3 = sValCount / (double)(x + 1) + (x + 1);
                        file.WriteLine(DateTime.Now.ToString() + "\n");
                        file.WriteLine("Sensitive Value Count:" + sValCount + "\t" + "NonSensitive Value Count:" + nsValCount + "\n");
                        file.WriteLine(cost1 + " : " + cost2);

                        if (//cost1 < cost2 && 
                            cost1 < cost2)
                        {
                            this.Y = (int)factors.y;
                            this.X = (int)factors.x;
                        }
                        //else if (cost2 < cost3 && cost2 < cost1)
                        //{
                        //    this.Y = x;
                        //    this.X = x;
                        //}
                        else
                        {
                            this.Y = y;
                            this.X = y;
                        }

                        file.WriteLine("X=" + this.X + " : " + "Y=" + this.Y);
                        file.WriteLine("\n\n");

                    }
                }
                
            }


            HandleSensitiveKeys();
            HandleNonSensitiveKeys();
            NormalizeSensitiveBuckets();
            SerializeStuff();

        }

        private void SerializeStuff()
        {
            SerializeData(SKeyBucketDict, "SKeyBucketDict_" + this.SensitivityPercent + ".osl");
            SerializeData(SKeyBucketsList, "SKeyBuckets_" + this.SensitivityPercent + ".osl");
            SerializeData(NSKeyBucketDict, "NSKeyBucketDict_" + this.SensitivityPercent + ".osl");
            SerializeData(NSKeyBucketsList, "NSKeyBuckets_" + this.SensitivityPercent + ".osl");
        }

        

        private void NormalizeSensitiveBuckets()
        {
            //find the bucket which has the max no of keys not values
            int Max = int.MinValue;
            foreach (int keyCount in SizeOfSensBuckets_Keys)
            {
                
                if (keyCount > Max)
                {
                    Max = keyCount;

                }
            }


            //go through each bucket and insert random stuff in
            // S table, the dict. SKeyBucketDict and the list SKeyBucketsList
            List<decimal> ListOfFakeKeysAdded = new List<decimal>();
            for (int i = 0; i < SKeyBucketsList.Count; i++)
            {
                while (SizeOfSensBuckets_Keys[i] != Max)
                {
                    SKeyBucketDict.Add(NextFakeKey, i);
                    SKeyBucketsList[i].Add(NextFakeKey);
                    ListOfFakeKeysAdded.Add(NextFakeKey);
                    NextFakeKey -= 1;
                    SizeOfSensBuckets_Keys[i] += 1;
                }
            }

            int FakesInserted = InsertIntoSensitiveTable(ListOfFakeKeysAdded, this.SensitiveTableName);
            Console.WriteLine(FakesInserted + " fake records inserted");
            

        }


        private int InsertIntoSensitiveTable(List<decimal> FakeKeys, String TableName)
        {

            int RowsAffected = 0;
            string InsertStatement =
                "INSERT INTO [GENERALCASE].[" + TableName + "] " +
                "([C_CUSTKEY], [C_NAME], [C_ADDRESS], [C_NATIONKEY], [C_PHONE], [C_ACCTBAL], [C_MKTSEGMENT], [C_COMMENT] ) " +
                "VALUES " +
                " ( @C_CUSTKEY, @C_NAME, @C_ADDRESS, @C_NATIONKEY, @C_PHONE, @C_ACCTBAL, @C_MKTSEGMENT, @C_COMMENT ) ; ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.Connection.Open();

                    cmd.CommandText = InsertStatement;

                    SqlParameter paramCustKey = cmd.CreateParameter();
                    paramCustKey.ParameterName = @"@C_CUSTKEY";
                    paramCustKey.DbType = DbType.Decimal;
                    paramCustKey.Precision = 11;
                    paramCustKey.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(paramCustKey);

                    SqlParameter paramName = cmd.CreateParameter();
                    paramName.ParameterName = @"@C_NAME";
                    paramName.DbType = DbType.String;
                    paramName.Direction = ParameterDirection.Input;
                    paramName.Size = 25;
                    cmd.Parameters.Add(paramName);

                    SqlParameter paramAddress = cmd.CreateParameter();
                    paramAddress.ParameterName = @"@C_ADDRESS";
                    paramAddress.DbType = DbType.String;
                    paramAddress.Direction = ParameterDirection.Input;
                    paramAddress.Size = 40;
                    cmd.Parameters.Add(paramAddress);

                    SqlParameter paramNationKey = cmd.CreateParameter();
                    paramNationKey.ParameterName = @"@C_NATIONKEY";
                    paramNationKey.DbType = DbType.Decimal;
                    paramNationKey.Direction = ParameterDirection.Input;
                    paramNationKey.Precision = 11;
                    cmd.Parameters.Add(paramNationKey);

                    SqlParameter paramPhone = cmd.CreateParameter();
                    paramPhone.ParameterName = @"@C_PHONE";
                    paramPhone.DbType = DbType.String;
                    paramPhone.Direction = ParameterDirection.Input;
                    paramPhone.Size = 15;
                    cmd.Parameters.Add(paramPhone);

                    SqlParameter paramAcctBal = cmd.CreateParameter();
                    paramAcctBal.ParameterName = @"@C_ACCTBAL";
                    paramAcctBal.DbType = DbType.Decimal;
                    paramAcctBal.Direction = ParameterDirection.Input;
                    paramAcctBal.Precision = 12;
                    paramAcctBal.Scale = 2;
                    cmd.Parameters.Add(paramAcctBal);

                    SqlParameter paramMkt = cmd.CreateParameter();
                    paramMkt.ParameterName = @"@C_MKTSEGMENT";
                    paramMkt.DbType = DbType.String;
                    paramMkt.Direction = ParameterDirection.Input;
                    paramMkt.Size = 10;
                    cmd.Parameters.Add(paramMkt);

                    SqlParameter paramComment = cmd.CreateParameter();
                    paramComment.ParameterName = @"@C_COMMENT";
                    paramComment.DbType = DbType.String;
                    paramComment.Direction = ParameterDirection.Input;
                    paramComment.Size = 100;
                    cmd.Parameters.Add(paramComment);


                    foreach (decimal fakeKey in FakeKeys)
                    {
                        paramCustKey.Value = fakeKey;
                        paramName.Value = RandomString(25);
                        paramAddress.Value = RandomString(40);
                        paramNationKey.Value = fakeKey;
                        paramPhone.Value = RandomString(15);
                        paramAcctBal.Value = fakeKey;
                        paramMkt.Value = RandomString(10);
                        paramComment.Value = RandomString(100);


                        RowsAffected += cmd.ExecuteNonQuery();
                    }



                }
            }

            return RowsAffected;
        }


        private void HandleNonSensitiveKeys()
        {
            List<decimal> NSKeys = GetAllCustKeyFromNonSensitive();
            NSKeys.Sort();
            FillUpDictAndBucketsForNonSensitive(NSKeys);

        }
        
        private void FillUpDictAndBucketsForNonSensitive(List<decimal> NSKeys)
        {
            int BucketSize = -1;
            int NumberOfBuckets = -1;

            if (this.NSIsLarger == true)
            {
                BucketSize = this.X;

                NumberOfBuckets = (int)Math.Ceiling(
                    (double)new HashSet<decimal>(NSKeys).Count /
                    (double)BucketSize); //no of distinct values / bucket size
            }
            else
            {
                //case when S is larger, we are fixing the number of buckets as Y
                //bucket size is the dependent value
                NumberOfBuckets = this.Y;
                BucketSize = (int)Math.Ceiling(
                    (double)new HashSet<decimal>(NSKeys).Count /
                    (double)NumberOfBuckets);

            }

            for (int i = 0; i < NumberOfBuckets; i++)
            {
                NSKeyBucketsList.Add(new List<decimal>());
                SizeOfNonSensBuckets_Values.Add(0);
            }

            foreach (decimal AKey in NSKeys)
            {
                //check if a corresponding key exist in S side
                int BucketIndexFound = -1;
                if (SKeyBucketDict.TryGetValue(AKey, out BucketIndexFound) == true)
                {

                    //check if the key is already added to NS buckets by looking up in NS dictionary
                    if (NSKeyBucketDict.ContainsKey(AKey) == true)
                    {
                        //the key has already been processed
                    }
                    else
                    {
                        //find out the position of the key inside a bucket in the S side
                        int TargetBucket = SKeyBucketsList[BucketIndexFound].IndexOf(AKey);
                        NSKeyBucketDict.Add(AKey, TargetBucket);
                        NSKeyBucketsList[TargetBucket].Add(AKey);
                        SizeOfNonSensBuckets_Values[TargetBucket] += 1;
                    }

                }
                else
                {
                    //ignore in the first pass
                }

            }


            //second pass
            int NSBucket = 0;
            for (NSBucket = 0; NSBucket < SizeOfNonSensBuckets_Values.Count; NSBucket++)
            {
                if (SizeOfNonSensBuckets_Values[NSBucket] < BucketSize)
                    break;
            }
            foreach (decimal AKey in NSKeys)
            {

                //check if the key is already present in the dictionary
                if (NSKeyBucketDict.ContainsKey(AKey) == true)
                {
                    //this means that the key has alredy been processed in the first pass
                    //or its a duplicate key processed in the second pass

                }
                else
                {
                    NSKeyBucketDict.Add(AKey, NSBucket);
                    NSKeyBucketsList[NSBucket].Add(AKey);
                    SizeOfNonSensBuckets_Values[NSBucket] += 1;

                    //get the next empty bucket
                    for (; NSBucket < SizeOfNonSensBuckets_Values.Count; NSBucket++)
                    {
                        if (SizeOfNonSensBuckets_Values[NSBucket] < BucketSize)
                            break;
                    }
                }

            }


        }


        private List<decimal> GetAllCustKeyFromNonSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [GENERALCASE].["+this.NonSenstitveTableName+"] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;
            
        }



        private void HandleSensitiveKeys()
        {
            List<CustKeyAndCount> SKeysWithCount = GetAllCustKeyWithCountFromSensitive();
            FillUpDictAndBucketsForSensitive(SKeysWithCount);
        }
        
        private void FillUpDictAndBucketsForSensitive(List<CustKeyAndCount> SKeysWithCount)
        {
            int numberOfSensitiveBuckets = -1;
            if(this.NSIsLarger  == true)
            {
                numberOfSensitiveBuckets = this.X;
            }
            else
            {
                numberOfSensitiveBuckets = (int)Math.Ceiling((double)SKeysWithCount.Count / (double)this.Y);
                //number of values in sensitive / max allowed per bucket which is Y
            }

            //create the buckets
            for (int i = 0; i < numberOfSensitiveBuckets; i++)
            {
                SKeyBucketsList.Add(new List<decimal>());
                SizeOfSensBuckets_Keys.Add(0);
            }
            
            foreach (CustKeyAndCount aKey in SKeysWithCount)
            {
                int k = findAppropriateBucket();
                SKeyBucketDict.Add(aKey.key, k);
                SKeyBucketsList[k].Add(aKey.key);
                SizeOfSensBuckets_Keys[k] += aKey.count;
            }
        }



        private int findAppropriateBucket()
        {
            //find the bucket which has the least number of keys
            int min = int.MaxValue;
            int minIndex = -1;
            int alloweedNumberOfValuesPerBucket = this.Y;
            //this.Y remains same whether we have ns > s or ns<s
            //in the later this is actually enforced

            for(int i=0; i< SizeOfSensBuckets_Keys.Count; i++)
            {
                int t = SizeOfSensBuckets_Keys[i];
                if(
                    t<min && //we have found the new minimum
                    SKeyBucketsList[i].Count < alloweedNumberOfValuesPerBucket//check that we have not crossed the values limit per bucket
                    )
                {
                    min = t;
                    minIndex = i;
                }
            }

            return minIndex;
            
        }

        /*
         * This function will return all distinct keys with count for each from the S table
         */
        private List<CustKeyAndCount> GetAllCustKeyWithCountFromSensitive()
        {
            List<CustKeyAndCount> AllCustKeys = new List<CustKeyAndCount>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY], [CNT] FROM "+
                "(SELECT C_CUSTKEY, COUNT(C_CUSTKEY) AS CNT FROM [GENERALCASE].["+  this.SensitiveTableName+"] "+
                "GROUP BY C_CUSTKEY) T1 "+
                "ORDER BY T1.CNT DESC; ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            CustKeyAndCount ckc = new CustKeyAndCount()
                            {
                                key = reader.GetDecimal(0),
                                count = reader.GetInt32(1)
                            };
                            AllCustKeys.Add(ckc);

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }

        /*
         * This function will return the number of distinct customer keys from the table passed as paramater
         */ 
        private long GetDistinctCustCount(string TableName)
        {
            long DistinctVals = -1;
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT COUNT(DISTINCT(C_CUSTKEY)) FROM GENERALCASE."+ TableName,
                new SqlConnection(connectionString));

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            DistinctVals = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
                

            return DistinctVals;
        }


        //static string connectionString = @"GeneralCase.Properties.Settings.SQLServerConn";
        static string connectionString = "";
        static string connectionStringWithoutEncryption = "";
        public static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }

        public static void SetupConnectionWithoutEncryption()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionStringWithoutEncryption = sConnection;


            Console.WriteLine(connectionStringWithoutEncryption);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionStringWithoutEncryption);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            //connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionStringWithoutEncryption = connStringBuilder.ConnectionString;
        }


        private void SerializeData(Object obj, string FileName)
        {
            Stream stream = File.Open(FileName, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();

            Console.WriteLine("Writing to file " + FileName);
            bformatter.Serialize(stream, obj);
            stream.Close();
        }

        public string RandomString(int length)
        {
            
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

    class CustKeyAndCount
    {
        public decimal key { get; set; }
        public int count { get; set; }

    }
}
