﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCase.Benchmark
{
    class EstimateBeta
    {

        private static int NUMBER_OF_SIMULATION = 50;
        private int simulationNumber = -1;
        private decimal[] keys = new decimal[NUMBER_OF_SIMULATION];
        private long[] timeEncryptedNonDet = new long[NUMBER_OF_SIMULATION];
        private long[] timeEncryptedDet = new long[NUMBER_OF_SIMULATION];
        private long[] timePlainText = new long[NUMBER_OF_SIMULATION];

        private static string plainTextTable = "CUSTOMER_TEMP";
        private static string encryptedNonDet = "FULLTABLE_ENCRYPTED";
        private static string encryptedDet = "FULLTABLE_ENCRYPTED_DET";


        public void print()
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter("BetaValues.txt"))
            {
                for (int i = 0; i < NUMBER_OF_SIMULATION; i++)
                {
                    file.WriteLine(
                        keys[i] + "\t" +
                        timePlainText[i] + "\t" +
                        timeEncryptedDet[i] + "\t" +
                        timeEncryptedNonDet[i]

                        );
                }
            }
        }

        public void run()
        {

            EstimateBeta.SetupConnection();
            Random rnd = new Random();
            for (int i = 0; i < NUMBER_OF_SIMULATION; i++)
            {
                this.simulationNumber = i;
                decimal CustKey = rnd.Next(1, 100000 + 1);
                keys[i] = CustKey;
                Console.WriteLine(i + ": Cust Key to find: " + CustKey);
                CleanBuffers();
                PointQuery(CustKey, true);
                CleanBuffers();
                PointQuery(CustKey, false);
                CleanBuffers();
                EncryptedNonDet(CustKey);
            }


        }





        List<Customer> PointQuery(decimal KeyToSearch, bool isEncrypted)
        {
            String tableName = "";
            long[] timing = null;
            if (isEncrypted == true)
            {
                tableName = EstimateBeta.encryptedDet;
                timing = this.timeEncryptedDet;
            }
            else
            {
                tableName = EstimateBeta.plainTextTable;
                timing = this.timePlainText;
            }

            string PartialQuery =
                "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM [GENERALCASE].[" + tableName + "] WHERE ";

            List<Customer> CustomerList = new List<Customer>();//a ball mark initial capacity
            Customer customer;

            string PlaceHolder = "@C_CUSTKEY";
            PartialQuery += "[C_CUSTKEY] = " + PlaceHolder;


            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));
            sqlCmd.CommandTimeout = 600;

            SqlParameter ParamCustKey = new SqlParameter(PlaceHolder, KeyToSearch);
            ParamCustKey.DbType = System.Data.DbType.Decimal;
            ParamCustKey.Direction = ParameterDirection.Input;
            //paramCustKey.SqlDbType = SqlDbType.Decimal;
            ParamCustKey.Precision = 11;
            sqlCmd.Parameters.Add(ParamCustKey);


            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                timing[this.simulationNumber] += sw.ElapsedMilliseconds;
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }


        private void EncryptedNonDet(decimal KeyToSearch)
        {
            List<Byte[]> rids = GetRIDFromFullSensitiveTable(KeyToSearch);
            GetTuplesFromFullSensitiveTableByRID(rids);

        }
        private List<Customer> GetTuplesFromFullSensitiveTableByRID(List<byte[]> allRIDs)
        {

            string PartialQuery =
            "SELECT [C_CUSTKEY] , [C_NAME] , [C_ACCTBAL] FROM ( " +
            "SELECT C_CUSTKEY, C_NAME , C_ACCTBAL , %%physloc%% as RID FROM GENERALCASE." + EstimateBeta.encryptedNonDet +
            " ) AS T WHERE ";



            List<Customer> CustomerList = new List<Customer>();
            Customer customer;

            int ParamCount = allRIDs.Count;


            string PlaceHolder = "@RID";
            for (int i = 0; i < ParamCount; i++)
            {
                if (i == 0)
                    PartialQuery += "[RID] = " + PlaceHolder + i + " ";
                else
                    PartialQuery += "OR [RID] = " + PlaceHolder + i + " ";

            }

            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));
            sqlCmd.CommandTimeout = 600;

            for (int i = 0; i < ParamCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder + i, allRIDs[i]);
                ParamCustKey.DbType = System.Data.DbType.Binary;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                //ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                timeEncryptedNonDet[simulationNumber] += sw.ElapsedMilliseconds;
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }

        private List<Byte[]> GetRIDFromFullSensitiveTable(decimal CustKeyToFind)
        {

            List<Byte[]> RIDList = new List<Byte[]>();


            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] , %%physloc%% AS [RID] FROM [GENERALCASE].[" + EstimateBeta.encryptedNonDet + "] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                sqlCmd.CommandTimeout = 600;
                Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (CustKeyToFind == reader.GetDecimal(0))
                            {
                                RIDList.Add((byte[])reader[1]);
                            }


                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw1.Stop();
                timeEncryptedNonDet[simulationNumber] += sw1.ElapsedMilliseconds;
            }



            return RIDList;


        }


        static string connectionString = @"GeneralCase.Properties.Settings.SQLServerConn";

        public static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }




        private void CleanBuffers()
        {
            string FlushStatement = "DBCC DROPCLEANBUFFERS; ";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.Connection.Open();

                    cmd.CommandText = FlushStatement;

                    cmd.ExecuteNonQuery();

                    //Console.WriteLine("Buffer Cleaned");
                }
            }
        }
    }
}