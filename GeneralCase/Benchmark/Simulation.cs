﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GeneralCase.Benchmark
{
    public class Simulation
    {

        private static int NUMBER_OF_SIMULATION = 50;
        private int currentSimulation = -1;

        private long[] Encrypted_Server = new long[NUMBER_OF_SIMULATION];
        private long[] Encrypted_Client = new long[NUMBER_OF_SIMULATION];
        private long[] NonEncryptedTimes = new long[NUMBER_OF_SIMULATION];
        private long[] NoBuckets_Server = new long[NUMBER_OF_SIMULATION];
        private long[] NoBuckets_Client = new long[NUMBER_OF_SIMULATION];
        private decimal[] Keys = new decimal[NUMBER_OF_SIMULATION];

        private Dictionary<decimal, int> NSKeyBucketDict;
        private List<List<decimal>> NSKeyBuckets;
        private Dictionary<decimal, int> SKeyBucketDict;
        private List<List<decimal>> SKeyBuckets;

        private string sensitiveTableName = "";
        private string nonSensitiveTableName = "";
        private string sensitivityPercent = "";
        private string ResultsFileName = "";
        private bool FoundInSensitive = false;


        public void perform(string sensitiveTableName, string nonSensitiveTableName , string sensitivityPercent)
        {
            this.sensitiveTableName = sensitiveTableName;
            this.nonSensitiveTableName = nonSensitiveTableName;
            this.sensitivityPercent = sensitivityPercent;
            this.ResultsFileName = "Timing_" + this.sensitivityPercent + ".tsv";

            Random rnd = new Random();

            LoadBucketsFromFile();
            for (int i = 0; i < NUMBER_OF_SIMULATION; i++)
            {
                currentSimulation = i;
                //Console.WriteLine("Enter the Cutomer key to Search");
                //decimal CustKey = Convert.ToDecimal(Console.ReadLine());
                decimal CustKey = rnd.Next(1, 100000 + 1);
                Keys[currentSimulation] = CustKey;
                Console.WriteLine(i+": Cust Key to find: " + CustKey);
                if (SKeyBucketDict.ContainsKey(CustKey))
                {
                    //the key exists in sensitive bucket.
                    //so the bucket number is fixed for both sensitive and non sensitive side
                    this.FoundInSensitive = true;

                }
                else
                {
                    this.FoundInSensitive = false;

                }
                FetchSensitiveTuples(CustKey);
                FetchNonSensitiveTuples(CustKey);
                SelectCustomersByIds(CustKey);
                CleanBuffers();
            }
            PrintTimings();

        }


        private void SelectCustomersByIds(decimal CustKey)
        {
            List<Byte[]> rids = GetRIDFromFullSensitiveTable(CustKey);
            GetTuplesFromFullSensitiveTableByRID(rids);
            
        }
        private List<Customer> GetTuplesFromFullSensitiveTableByRID(List<byte[]> allRIDs)
        {

            string PartialQuery =
            "SELECT [C_CUSTKEY] , [C_NAME] , [C_ACCTBAL] FROM ( " +
            "SELECT C_CUSTKEY, C_NAME , C_ACCTBAL , %%physloc%% as RID FROM GENERALCASE.FULLTABLE_ENCRYPTED " +
             ") AS T WHERE ";
            


            List<Customer> CustomerList = new List<Customer>();
            Customer customer;

            int ParamCount = allRIDs.Count;


            string PlaceHolder = "@RID";
            for (int i = 0; i < ParamCount; i++)
            {
                if (i == 0)
                    PartialQuery += "[RID] = " + PlaceHolder + i + " ";
                else
                    PartialQuery += "OR [RID] = " + PlaceHolder + i + " ";

            }

            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));
            sqlCmd.CommandTimeout = 600;

            for (int i = 0; i < ParamCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder + i, allRIDs[i]);
                ParamCustKey.DbType = System.Data.DbType.Binary;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                //ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                NoBuckets_Server[currentSimulation] += sw.ElapsedMilliseconds;
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }

        private List<Byte[]> GetRIDFromFullSensitiveTable(decimal CustKeyToFind)
        {

            List<Byte[]> RIDList = new List<Byte[]>();

            List<decimal> tempKeys = new List<decimal>();
            List<Byte[]> tempRids = new List<byte[]>();
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] , %%physloc%% AS [RID] FROM [GENERALCASE].[FULLTABLE_ENCRYPTED] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                sqlCmd.CommandTimeout = 600;
                Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            tempKeys.Add(reader.GetDecimal(0));
                            tempRids.Add((byte[])reader[1]);

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw1.Stop();
                NoBuckets_Server[this.currentSimulation] += sw1.ElapsedMilliseconds;

                Stopwatch sw2 = System.Diagnostics.Stopwatch.StartNew();
                for(int i=0; i<tempKeys.Count; i++)
                {
                    if(tempKeys[i] == CustKeyToFind)
                    {
                        RIDList.Add(tempRids[i]);
                    }
                }
                sw2.Stop();
                NoBuckets_Client[this.currentSimulation] += sw2.ElapsedMilliseconds;

            }

            return RIDList;


        }

        private void FetchNonSensitiveTuples(decimal key)
        {

            int BucketNumber = -1;
            if (this.FoundInSensitive == true)
            {
                int SBucketNumber = SKeyBucketDict[key];
                int PosnInSBucket = SKeyBuckets[SBucketNumber].FindIndex(x => x == key);
                BucketNumber = PosnInSBucket;
            }
            else
            {
                //It is possible that key is neither present in S nor in NS
                 if(
                    NSKeyBucketDict.TryGetValue(key, out BucketNumber)
                    == false)
                {
                    //get some random bucket
                    BucketNumber = new Random().Next(0, NSKeyBuckets.Count);
                }
            }

            List<decimal> AllKeys = NSKeyBuckets[BucketNumber];
            List<Customer> AllCustomers = SelectCustomersByIds(AllKeys);
        }


        List<Customer> SelectCustomersByIds(List<decimal> CustKeys)
        {

            string PartialQuery =
                "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM [GENERALCASE].["+this.nonSensitiveTableName+"] WHERE ";
            
            List<Customer> CustomerList = new List<Customer>(CustKeys.Count*2);//a ball mark initial capacity
            Customer customer;

            int KeyCount = CustKeys.Count;


            string PlaceHolder = "@C_CUSTKEY";
            for (int i = 0; i < KeyCount; i++)
            {
                if (i == 0)
                    PartialQuery += "[C_CUSTKEY] = " + PlaceHolder + i + " ";
                else
                    PartialQuery += "OR [C_CUSTKEY] = " + PlaceHolder + i + " ";

            }

            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));
            sqlCmd.CommandTimeout = 600;

            for (int i = 0; i < KeyCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder + i, CustKeys[i]);
                ParamCustKey.DbType = System.Data.DbType.Decimal;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                NonEncryptedTimes[currentSimulation] += sw.ElapsedMilliseconds;
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }


        private void FetchSensitiveTuples(decimal key)
        {
            int BucketNumber = -1;

            if (this.FoundInSensitive == true)
            {
                BucketNumber = SKeyBucketDict[key];
            }
            else
            {
                BucketNumber = new Random().Next(0, SKeyBuckets.Count);
            }
            List<decimal> AllKeys = SKeyBuckets[BucketNumber];
            List<Byte[]> AllRIDs = GetRIDFromSensitive(new HashSet<decimal>(AllKeys));
            GetTuplesFromSensitiveByRID(AllRIDs);


        }

        private List<Customer> GetTuplesFromSensitiveByRID(List<byte[]> allRIDs)
        {

            string PartialQuery =
            "SELECT [C_CUSTKEY] , [C_NAME] , [C_ACCTBAL] FROM ( " +
            "SELECT C_CUSTKEY, C_NAME , C_ACCTBAL , %%physloc%% as RID FROM GENERALCASE." + this.sensitiveTableName+
             ") AS T WHERE ";
            


            List<Customer> CustomerList = new List<Customer>();
            Customer customer;

            int ParamCount = allRIDs.Count;


            string PlaceHolder = "@RID";
            for (int i = 0; i < ParamCount; i++)
            {
                if (i == 0)
                    PartialQuery += "[RID] = " + PlaceHolder + i + " ";
                else
                    PartialQuery += "OR [RID] = " + PlaceHolder + i + " ";

            }

            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));
            sqlCmd.CommandTimeout = 600;

            for (int i = 0; i < ParamCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder + i, allRIDs[i]);
                ParamCustKey.DbType = System.Data.DbType.Binary;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                //ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                Encrypted_Server[currentSimulation] += sw.ElapsedMilliseconds;
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }


        private List<Byte[]> GetRIDFromSensitive(HashSet<decimal> CustKeySet)
        {

            List<Byte[]> RIDList = new List<Byte[]>();

            List<decimal> tempKeys = new List<decimal>();
            List<Byte[]> tempRids = new List<byte[]>();
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] , %%physloc%% AS [RID] FROM [GENERALCASE].["+this.sensitiveTableName+"] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                sqlCmd.CommandTimeout = 600;
                Stopwatch sw1 = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            tempKeys.Add(reader.GetDecimal(0));
                            tempRids.Add((byte[])reader[1]);

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw1.Stop();
                Encrypted_Server[this.currentSimulation] += sw1.ElapsedMilliseconds;

                Stopwatch sw2 = System.Diagnostics.Stopwatch.StartNew();
                for (int i = 0; i < tempKeys.Count; i++)
                {
                    if (CustKeySet.Contains(tempKeys[i]))
                    {
                        RIDList.Add(tempRids[i]);
                    }
                }
                sw2.Stop();
                Encrypted_Client[this.currentSimulation] += sw2.ElapsedMilliseconds;

            }

            return RIDList;


        }

        private void PrintTimings()
        {
            Console.WriteLine("Writing The Timings to file:" + this.ResultsFileName);
            
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(ResultsFileName))
            {
                file.WriteLine("SS\tSC\tNS\tNBS\tNBC");
                for (int i = 0; i < NUMBER_OF_SIMULATION; i++)
                {
                    file.WriteLine(
                        Keys[i] + "\t" +
                        Encrypted_Server[i] + "\t" +
                        Encrypted_Client[i] + "\t" +
                        NonEncryptedTimes[i] + "\t" +
                        NoBuckets_Server[i] + "\t" +
                        NoBuckets_Client[i]);
                }
                file.WriteLine(Environment.NewLine);
                file.WriteLine(Environment.NewLine + "Average Timings:");
                file.WriteLine(Environment.NewLine);
                file.WriteLine(
                    Encrypted_Server.Average() + "\t" +
                    Encrypted_Client.Average() + "\t" +
                    NonEncryptedTimes.Average() + "\t" +
                    NoBuckets_Server.Average() + "\t" +
                    NoBuckets_Client.Average());
            }
            Console.WriteLine("Writing Finished....");

        }

        private void LoadBucketsFromFile()
        {
            NSKeyBucketDict = (Dictionary<decimal, int>)Deserialize("NSKeyBucketDict_" + this.sensitivityPercent + ".osl");
            NSKeyBuckets = (List<List<decimal>>)Deserialize("NSKeyBuckets_" + this.sensitivityPercent + ".osl");
            SKeyBucketDict = (Dictionary<decimal, int>)Deserialize("SKeyBucketDict_" + this.sensitivityPercent + ".osl");
            SKeyBuckets = (List<List<decimal>>)Deserialize("SKeyBuckets_" + this.sensitivityPercent + ".osl");
        }

        private Object Deserialize(string FileName)
        {

            //Open the file written above and read values from it.
            Stream stream = File.Open(FileName, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            Object obj = bformatter.Deserialize(stream);
            stream.Close();
            return obj;

        }

        static string connectionString = @"GeneralCase.Properties.Settings.SQLServerConn";
        
        public static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }


        

        private void CleanBuffers()
        {
            string FlushStatement = "DBCC DROPCLEANBUFFERS; ";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand cmd = connection.CreateCommand())
                {
                    cmd.Connection.Open();

                    cmd.CommandText = FlushStatement;

                    cmd.ExecuteNonQuery();

                    //Console.WriteLine("Buffer Cleaned");
                }
            }
        }
    }
}
