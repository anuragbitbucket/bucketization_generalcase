﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnequalPartition.Utility
{
    class Factors
    {
        public long x { get; set; }
        public long y { get; set; }

        public override string ToString()
        {
            return x+":"+y;
        }
    }
}
